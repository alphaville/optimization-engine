<p align="center">
  <a href="https://alphaville.github.io/optimization-engine/">
    <img alt="OpEn logo" src="https://pbs.twimg.com/media/D1d8iOuXQAUFKJT.png:large">
  </a>
</p>

<p align="center">
  <a href="https://twitter.com/intent/tweet?text=Fast%20and%20accurate%20nonconvex%20optimization&url=https://alphaville.github.io/optimization-engine/&via=isToxic&hashtags=optimization,rustlang,matlab,python">
    <img alt="Tweet" src="https://img.shields.io/twitter/url/http/shields.io.svg?style=social">
  </a>
  <a href="https://travis-ci.org/alphaville/optimization-engine">
    <img alt="build status" src="https://travis-ci.org/alphaville/optimization-engine.svg?branch=master">
  </a>
  <a href="https://ci.appveyor.com/project/alphaville/optimization-engine/branch/master">
    <img alt="build status" src="https://ci.appveyor.com/api/projects/status/fy9tr4xmqq3ka4aj/branch/master?svg=true">
  </a>
</p>

<p align="center">
  <a href="https://lbesson.mit-license.org/">
    <img alt="MIT license" src="https://img.shields.io/badge/License-MIT-blue.svg">
  </a>
  <a href="https://github.com/alphaville/optimization-engine/blob/master/LICENSE-APACHE">
    <img alt="Apache v2 license" src="https://img.shields.io/badge/License-Apache%20v2-blue.svg">
  </a>
</p>

<p align="center">
  <a href="https://gitter.im/alphaville/optimization-engine?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge">
    <img alt="Gitter" src="https://badges.gitter.im/alphaville/optimization-engine.svg">
  </a>
  <a href="https://discord.gg/mfYpn4V">
    <img alt="Chat on Discord" src="https://img.shields.io/badge/chat-on%20discord-gold.svg">
  </a>
</p>

Optimization Engine (OpEn) is a solver for embedded nonconvex optimization.

**Documentation available at** [**alphaville.github.io/optimization-engine**](https://alphaville.github.io/optimization-engine/)

**Source code available at**] [**github.com/alphaville/optimization-engine**](https://github.com/alphaville/optimization-engine)

